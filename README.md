# CoviRes
Telegram bot that provides the user with tweets containing information on vital resources.

### To run the bot on local server

1. `pip install -r requirements.txt`
2. Create a virtual environment and run it
3. `python bot.py`

Now in telegram,

1. Search @CoviResBot and type `/start` (This needs to be done only if you are using the bot for the first time).
2. Then type `/city <CITY NAME>` to choose the city.
3. Then type `/menu` and choose an option from the available ones.
4. Use command `/help` to get further more information (if required).
